<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'platform', 'platform_version', 'browser', 'browser_version', 'is_desktop', 'is_mobile', 'language', 'is_trusted', 'is_unstrusted'
    ];

    protected $table = 'devices';

    // Relación
    public function usuario()
    {
        return $this->belongsTo('App\User','user_id');
    }  

}