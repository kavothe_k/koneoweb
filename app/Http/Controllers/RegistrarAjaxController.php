<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class RegistrarAjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function registrar()
    {
        return view('usuario');
    }

    public function index(Request $request)

    {

        $users = User::latest()->paginate(5);

        return response()->json($users);

    }

    public function store(Request $request)

    {
        $user = new User;
        $user->name = $request->get('name');
        $user->email = $request->get('email'); 
        $user->password = bcrypt($request->get('password'));
        
        if ($user->save()){
            $result = true;
        }else{
            $result = false;
        }

        return response()->json($result);

    }

    public function update(Request $request, $id)

    {

        $user = User::find($id);

        $user->name = $request->get('name');
        $user->email = $request->get('email'); 
        if($user->password != $request->password){
            $user->password = bcrypt($request->get('password'));
        }else{
            $user->password = $request->password; 
        }
        
        if($user->update()){
            $result = true;
        }else{
            $result = false;
        }


        return response()->json($result);

    }

    public function destroy($id)

    {

        User::find($id)->delete();

        return response()->json(['done']);

    }

}
