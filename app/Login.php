<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ip_address', 'type', 'user_id', 'device_id'
    ];

    protected $table = 'logins';

    // Relación
    public function usuario()
    {
        return $this->belongsTo('App\User','user_id');
    }  

    public function device()
    {
        return $this->belongsTo('App\Device','device_id');
    }

}
