$.ajaxSetup({

    headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

});


/* Create new Item */

function radio(obj){

    var form_action = $("#create").find("form").attr("action");

    var usuario_id = $("#create").find("input[name='usuario_id']").val();

    var pregunta_id = $(obj).data('pregunta');
    
    var modulo_id = $("#create").find("input[name='modulo_id']").val();

    var respuesta_id = $(obj).val();

    var correcta = $(obj).data('correcta');


    $.ajax({

         dataType: 'json',

         type:'POST',

         url: form_action,

        data:{usuario_id:usuario_id, pregunta_id:pregunta_id, modulo_id:modulo_id, respuesta_id:respuesta_id, correcta:correcta}

    }).done(function(data){

         toastr.info('Respuesta Guardada.', '', {timeOut: 5000});

    });

}



/* Remove Item */

// $("body").on("click",".remove-item",function(){

//     var id = $(this).parent("td").data('id');

//     var c_obj = $(this).parents("tr");

//     $.ajax({

//         dataType: 'json',

//         type:'delete',

//         url: url + '/' + id,

//     }).done(function(data){

//         c_obj.remove();

//         toastr.success('Módulo Eliminado Correctamente.', 'Success Alert', {timeOut: 5000});

//         getPageData();

//     });

// });


// /* Edit Item */

// $("body").on("click",".edit-item",function(){

//     var id = $(this).parent("td").data('id');

//     var pregunta_id = $(this).parent("td").prev("td").prev("td").text();

//     var respuesta = $(this).parent("td").prev("td").text();
    
//     var correcta = $(this).parent("td").prev("td").text();

//     var orden = $(this).parent("td").prev("td").text();

//     $("#edit-item").find("select[name='pregunta_id']").val(pregunta);

//     $("#edit-item").find("input[name='respuesta']").val(respuesta);
    
//     $("#edit-item").find("input[name='correcta']").val(correcta);

//     $("#edit-item").find("input[name='orden']").val(orden);

//     $("#edit-item").find("form").attr("action",url + '/' + id);

// });


// /* Updated new Item */

// $(".crud-submit-edit").click(function(e){

//     e.preventDefault();

//     var form_action = $("#edit-item").find("form").attr("action");

//     var pregunta_id = $("#edit-item").find("select[name='pregunta_id']").val();

//     var respuesta = $("#edit-item").find("input[name='respuesta']").val();
    
//     var correcta = $("#edit-item").find("input[name='correcta']").val();
    
//     var orden = $("#edit-item").find("input[name='orden']").val();


//     $.ajax({

//         dataType: 'json',

//         type:'PUT',

//         url: form_action,

//         data:{pregunta_id:pregunta_id, respuesta:respuesta, correcta:correcta, orden:orden}

//     }).done(function(data){

//         getPageData();

//         $(".modal").modal('hide');

//         toastr.success('Módulo Actualizado Correctamente.', 'Success Alert', {timeOut: 5000});

//     });

// });