var page = 1;

var current_page = 1;

var total_page = 0;

var is_ajax_fire = 0;

var availableTags;


manageData();


/* manage data list */

function manageData() {

    $.ajax({

        dataType: 'json',

        url: url,

        data: {page:page}

    }).done(function(data){

    	total_page = data.last_page;

    	current_page = data.current_page;


    	$('#pagination').twbsPagination({

	        totalPages: total_page,

	        visiblePages: current_page,

	        onPageClick: function (event, pageL) {

	        	page = pageL;

                if(is_ajax_fire != 0){

	        	  getPageData();

                }

	        }

	    });


    	manageRow(data.data);

        is_ajax_fire = 1;

    });

    manageModuloList();

}

function manageModuloList() {

    var selectId = '';
    var selectName = '';

    $.ajax({

        dataType: 'json',

        url: 'moduloList'

    }).done(function(data){


        $.each( data, function( key, value ) {

            selectId = value.id;

            selectName = value.nombre;

            $("#selectModulo" ).append('<option value="'+selectId+'">'+selectName+'</option>');
            $("#selectModuloEditar" ).append('<option value="'+selectId+'">'+selectName+'</option>');
        });
        
    });

}


$.ajaxSetup({

    headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

});


/* Get Page Data*/

function getPageData() {

	$.ajax({

    	dataType: 'json',

    	url: url,

    	data: {page:page}

	}).done(function(data){

		manageRow(data.data);

	});

}


/* Add new Item table row */

function manageRow(data) {

	var	rows = '';

	$.each( data, function( key, value ) {

	  	rows = rows + '<tr>';

	  	rows = rows + '<td>'+value.modulo_text+'</td>';

        rows = rows + '<td>'+value.pregunta+'</td>';

        rows = rows + '<td>'+value.orden+'</td>';

	  	rows = rows + '<td data-id="'+value.id+'">';

                rows = rows + '<button data-toggle="modal" data-orden="'+value.orden+'" data-pregunta="'+value.pregunta+'" data-modulo_id="'+value.modulo_id+'" data-target="#edit-item" class="btn btn-primary edit-item">Editar</button> ';

                rows = rows + '<button class="btn btn-danger remove-item">Borrar</button>';

                rows = rows + '</td>';

	  	rows = rows + '</tr>';

	});


	$("tbody").html(rows);

}


/* Create new Item */

$(".crud-submit").click(function(e){

    e.preventDefault();

    var form_action = $("#create-item").find("form").attr("action");

    var modulo_id = $("#create-item").find("select[name='modulo_id']").val();

    var pregunta = $("#create-item").find("textarea[name='pregunta']").val();
    
    var orden = $("#create-item").find("input[name='orden']").val();


    $.ajax({

        dataType: 'json',

        type:'POST',

        url: form_action,

        data:{modulo_id:modulo_id, pregunta:pregunta, orden:orden}

    }).done(function(data){

        getPageData();

        $(".modal").modal('hide');

        toastr.success('Modulo Creado Correctamente.', 'Success Alert', {timeOut: 5000});

    });


});


/* Remove Item */

$("body").on("click",".remove-item",function(){

    var id = $(this).parent("td").data('id');

    var c_obj = $(this).parents("tr");

    $.ajax({

        dataType: 'json',

        type:'delete',

        url: url + '/' + id,

    }).done(function(data){

        c_obj.remove();

        toastr.success('Módulo Eliminado Correctamente.', 'Success Alert', {timeOut: 5000});

        getPageData();

    });

});


/* Edit Item */

$("body").on("click",".edit-item",function(){

    var id = $(this).parent("td").data('id');

    var modulo_id = $(this).data('modulo_id');

    var pregunta = $(this).data('pregunta');
    
    var orden = $(this).data('orden');

    $("#edit-item").find("select[name='modulo_id']").each(function(){

        $(this).children("option").each(function(){
        
            if($(this).val() == modulo_id){
                $(this).attr('selected', 'selected');

            }
        });
    });

    $("#edit-item").find("textarea[name='pregunta']").val(pregunta);
    
    $("#edit-item").find("input[name='orden']").val(orden);

    $("#edit-item").find("form").attr("action",url + '/' + id);

});


/* Updated new Item */

$(".crud-submit-edit").click(function(e){

    e.preventDefault();

    var form_action = $("#edit-item").find("form").attr("action");

    var modulo_id = $("#edit-item").find("select[name='modulo_id']").val();

    var pregunta = $("#edit-item").find("textarea[name='pregunta']").val();
    
    var orden = $("#edit-item").find("input[name='orden']").val();


    $.ajax({

        dataType: 'json',

        type:'PUT',

        url: form_action,

        data:{modulo_id:modulo_id, pregunta:pregunta, orden:orden}

    }).done(function(data){

        getPageData();

        $(".modal").modal('hide');

        toastr.success('Módulo Actualizado Correctamente.', 'Success Alert', {timeOut: 5000});

    });

});