@extends('layouts.app')

@section('content')
<div class="content">
	<div class="wizard" data-initialize="wizard" id="myWizard">
		<div class="steps-container">
			<ul class="steps">
				<li data-step="1" data-name="videos" class="active">
					<span class="badge">Videos</span>
					<span class="chevron"></span>
				</li>
				<li data-step="2" data-name="campaign">
					<span class="badge">1</span>
					<span class="chevron"></span>
				</li>
				<li data-step="3">
					<span class="badge">2</span>
					<span class="chevron"></span>
				</li>
				<li data-step="4" data-name="template">
					<span class="badge">3</span>
					<span class="chevron"></span>
				</li>
				<li data-step="5" data-name="template">
					<span class="badge">4</span>
					<span class="chevron"></span>
				</li>
				<li data-step="6" data-name="template">
					<span class="badge">5</span>
					<span class="chevron"></span>
				</li>
				<li data-step="7" data-name="template">
					<span class="badge">6</span>
					<span class="chevron"></span>
				</li>
				<li data-step="8" data-name="template">
					<span class="badge">7</span>
					<span class="chevron"></span>
				</li>
				<li data-step="9" data-name="template">
					<span class="badge">8</span>
					<span class="chevron"></span>
				</li>
				<li data-step="10" data-name="template">
					<span class="badge">9</span>
					<span class="chevron"></span>
				</li>
				<li data-step="11" data-name="template">
					<span class="badge">10</span>
					<span class="chevron"></span>
				</li>
				<li data-step="12" data-last="Complete">
					<span class="badge"></span>
					<span class="chevron"></span>
				</li>
			</ul>
		</div>
		<?php $c = 2; $checked= ''; ?>
		<div class="step-content" id="create">
			@foreach($quiz as $preguntas)
				<div class="step-pane sample-pane bg-info alert" data-step="1">
					@if($id == 1)
					<div class="col-lg-6 col-md-6 col-sm-6">
						<video src="/videos/Seguridad_de_la_Informacion.mp4" type="video/mp4" controls width="100%" height="100%"></video>
						
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<video src="/videos/Seguridad_de_la_Informacion.mp4" type="video/mp4" controls width="100%" height="100%"></video>
						
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<video src="/videos/Seguridad_de_la_Informacion.mp4" type="video/mp4" controls width="100%" height="100%"></video>
						
					</div>
					@endif
					@if($id == 2)
					
					@endif
					@if($id == 3)
					
					@endif
					@if($id == 4)
					
					@endif
					@if($id == 5)
					
					@endif
	            	
				</div>
				<div class="step-pane sample-pane alert" data-step="{{$c}}">
					<h4>{!! $preguntas->pregunta !!}</h4>
					<form action="{{ route('questionarioAjax.store') }}" method="POST">
						@foreach($preguntas->respuestas as $respuesta)
							@foreach($respuestaUsuario as $resUsu)
								<?php $checked = ''; ?>
								@if($resUsu->respuesta_id == $respuesta->id) 
					    			<?php $checked = 'checked=checked'; ?>
				    			@endif
					    	@endforeach
							<div class="radio">
								<label>
							    	<input onclick="radio(this)" type="radio" class="radios" name="respuesta_id" value="{{ $respuesta->id }}"{{$checked}} data-correcta="{{$respuesta->correcta}}" data-pregunta="{{$preguntas->id}}">{{ $respuesta->respuesta }}
							  	</label>	
							</div>
						@endforeach
						<input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="modulo_id" value="{{$preguntas->modulo->id}}">
						<input type="hidden" name="pregunta_id" value="{{$preguntas->id}}">
					</form>
				</div>
				<?php $c++ ?>
			@endforeach	
			<div class="step-pane sample-pane bg-info alert" data-step="12">
				<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header count" data-background-color="blue">
                           
                        </div>
                        <div class="card-content">
                            <p class="category">Total</p>
                            <p class="category"><span class="text-success"></span></p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">update</i> <a href="/questionario/1">Accede al modulo.</a>
                            </div>
                        </div>
                    </div>
            	</div>
			</div>
		</div>
		<div class="actions">
			<button type="button" class="btn btn-default btn-sm btn-prev">
				<span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
			<button type="button" class="btn btn-primary btn-sm btn-next" data-last="Complete">Next
				<span class="glyphicon glyphicon-arrow-right"></span>
			</button>
		</div>
	</div>
</div>
@push('scripts')
<script type="text/javascript">

   //$('#myWizard').wizard();
   $('#myWizard').on('actionclicked.fu.wizard', function (evt, data) {
		$.get("{{ route('questionarioAjaxTotal') }}", {id:$('#create').find("input[name='modulo_id']").val()}, function(data){
                $('.count').text(data.result+' %');
                $('.text-success').text('Tuviste '+data.count+' aciertos de '+data.totalPreguntas+' preguntas.');
            });
	// do something
	});

   $(document).on('click', '[data-toggle="lightbox"]', function(event) {
	    event.preventDefault();
	    $(this).ekkoLightbox();
	});


   var url = "<?php echo route('questionarioAjax.index')?>";

</script>
<script src="/js/questionario-ajax.js"></script>
@endpush
@endsection