@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            @if(Auth::user()->rol == "admin")
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Log</h4>
                            <p class="category">Último acceso de los usuarios</p>
                        </div>
                        <div class="card-content">
                            @foreach($logins as $login)
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="card card-stats">
                                        <div class="card-header" data-background-color="blue">
                                            Preguntas contestadas: {{ $login['count'] }} 
                                        </div>
                                        <div class="card-content">
                                            <p class="category"><strong>Usuario:</strong> <span class="text-success">{{ $login->usuario->name }}</span></p>
                                            <p class="category"><strong>Acceso:</strong> <span class="text-success">{{ $login->created_at }}</span></p>
                                            <p class="category"><strong>Plataforma:</strong> <span class="text-success">{{ $login->device['platform'] }}</span></p>
                                            <p class="category"><strong>Modulo:</strong><span class="text-success"> {{ $login->modulo_name }}</span></p>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if(Auth::user()->rol == "invitado")
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="blue">
                            {{$result1}} %
                        </div>
                        <div class="card-content">
                            <p class="category">Total</p>
                            <p class="category"><span class="text-success">Tuviste {{$countModulo1}} aciertos de {{$totalPreguntasModulo1}} preguntas.</span></p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <p><i class="material-icons">update</i> <a href="/questionario/1">Accede a Importancia de la Seguridad de la Información.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="purple">
                            {{$result2}} %
                        </div>
                        <div class="card-content">
                            <p class="category">Total</p>
                            <p class="category"><span class="text-success">Tuviste {{$countModulo2}} aciertos de {{$totalPreguntasModulo2}} preguntas.</span></p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <p><i class="material-icons">update</i> <a href="/questionario/2">Accede a Bases de Seguridad de la Información.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="green">
                            {{$result3}} %
                        </div>
                        <div class="card-content">
                            <p class="category">Total</p>
                            <p class="category"><span class="text-success">Tuviste {{$countModulo3}} aciertos de {{$totalPreguntasModulo3}} preguntas.</span></p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <p><i class="material-icons">update</i> <a href="/questionario/3">Accede a Principales Amenazas y Riesgos.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            {{$result4}} %
                        </div>
                        <div class="card-content">
                            <p class="category">Total</p>
                            <p class="category"><span class="text-success">Tuviste {{$countModulo4}} aciertos de {{$totalPreguntasModulo4}} preguntas.</span></p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <p><i class="material-icons">update</i> <a href="/questionario/4">Accede a Medidas preventivas de seguridad de información.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="red">
                            {{$result5}} %
                        </div>
                        <div class="card-content">
                            <p class="category">Total</p>
                            <p class="category"><span class="text-success">Tuviste {{$countModulo5}} aciertos de {{$totalPreguntasModulo5}} preguntas.</span></p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <p><i class="material-icons">update</i> <a href="/questionario/5">Accede a Escenarios de riesgo de seguridad de información.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@push('scripts')
@endpush
@endsection
