@extends('layouts.app')

@section('content')
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="orange">
                        <h4 class="title">Crear Respuestas</h4>
						<p class="category">Completa los campos</p>
                    </div>
                    <div class="card-content">
						<div class="row">
							<div class="col-lg-12 margin-tb">
								<div class="pull-right">
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-item">Crear Respuestas</button>
						        </div>
						    </div>
						</div>
						<table class="table table-bordered table-hover">
							<thead>
							    <tr>
				        			<th>Pregunta</th>
				        			<th>Respuesta</th>
				        			<th>Correcta</th>
				        			<th>Orden</th>
				        			<th>Action</th>
			        			</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<ul id="pagination" class="pagination-sm"></ul>
					    <!-- Create Item Modal -->
						<div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
						    	<div class="modal-content">
						    		<div class="modal-header">
						    			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						    			<h4 class="modal-title" id="myModalLabel">Crear Respuestas</h4>
								    </div>
						      		<div class="modal-body">
							      		<form data-toggle="validator" action="{{ route('respuestaAjax.store') }}" method="POST">
							      			<div class="form-group">
												<label class="control-label" for="title">Pregunta:</label>
												<select id="selectModulo" name="pregunta_id" class="form-control" data-error="Please enter name." required></select>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label" for="title">Respuesta:</label>
												<input type="text" name="respuesta" class="form-control" data-error="Please enter Pregunta." required />
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label" for="title">Correcta:</label>
												<input type="text" name="correcta" class="form-control" data-error="Please enter Orden." required />
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label" for="title">Orden:</label>
												<input type="text" name="orden" class="form-control" data-error="Please enter Orden." required />
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<button type="submit" class="btn crud-submit btn-success">Enviar</button>
											</div>
							      		</form>
					      			</div>
						    	</div>
						  	</div>
						</div>
						<!-- Edit Item Modal -->
						<div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
										<h4 class="modal-title" id="myModalLabel">Editar Respuesta</h4>
							      	</div>
							      	<div class="modal-body">
							      		<form data-toggle="validator" action="/respuestaAjax/14" method="put">
							      			<div class="form-group">
							      				<label class="control-label" for="title">Pregunta:</label>
												<select id="selectModulo" name="pregunta_id" class="form-control" data-error="Please enter name." required></select>
												
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label" for="title">Respuesta:</label>
												<input type="text" name="respuesta" class="form-control" data-error="Please enter Pregunta." required/>
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label" for="title">Correcta:</label>
												<input type="text" name="correcta" class="form-control" data-error="Please enter Orden." required />
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<label class="control-label" for="title">Orden:</label>
												<input type="text" name="orden" class="form-control" data-error="Please enter Orden." required />
												<div class="help-block with-errors"></div>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-success crud-submit-edit">Guardar</button>
											</div>
							      		</form>
							      	</div>
							    </div>
						    </div>
					    </div>
				    </div>
			    </div>
		    </div>
		</div>
	</div>
</div>
@push('scripts')
<script type="text/javascript">

   var url = "<?php echo route('respuestaAjax.index')?>";

</script>
<script src="/js/respuesta-ajax.js"></script>
@endpush
@endsection
