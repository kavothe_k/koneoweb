<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Segob') }}</title>

    <!-- Bootstrap core CSS     -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//www.fuelcdn.com/fuelux/3.13.0/css/fuelux.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.css" rel="stylesheet">

    <!--  Material Dashboard CSS    -->
    <link href="/css/material-dashboard.css" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="/css/demo.css" rel="stylesheet" />
    <link href="/css/charts.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,600,800,900" rel="stylesheet" type="text/css">
</head>
<body class="fuelux">
    <div class="wrapper">
        @if (Auth::user()->rol == "admin")
            <div class="sidebar" data-color="green" data-image="/img/sidebar-1.jpg">
                <!--
                    Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

                    Tip 2: you can also add an image using data-image tag
                -->

                <div class="logo">
                    <a href="http://globallynx.com" class="simple-text">
                        Globallynx
                    </a>
                </div>

                <div class="sidebar-wrapper">
                    <ul class="nav">
                        @if(($_SERVER["REQUEST_URI"]) == '/home') 
                        <li class="active">
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/usuario">
                                <i class="material-icons">person</i>
                                <p>Crear Usuario</p>
                            </a>
                        </li>
                        <li>
                            <a href="/modulo">
                                <i class="material-icons">create</i>
                                <p>Creación de Módulos</p>
                            </a>
                        </li>
                        <li>
                            <a href="/pregunta">
                                <i class="material-icons">question_answer</i>
                                <p>Creación de Preguntas</p>
                            </a>
                        </li>
                        <li>
                            <a href="/respuesta">
                                <i class="material-icons">content_paste</i>
                                <p>Creación de Respuestas</p>
                            </a>
                        </li>
                        @endif
                         @if(($_SERVER["REQUEST_URI"]) == '/usuario') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/usuario">
                                <i class="material-icons">person</i>
                                <p>Crear Usuario</p>
                            </a>
                        </li>
                        <li>
                            <a href="/modulo">
                                <i class="material-icons">create</i>
                                <p>Creación de Módulos</p>
                            </a>
                        </li>
                        <li>
                            <a href="/pregunta">
                                <i class="material-icons">question_answer</i>
                                <p>Creación de Preguntas</p>
                            </a>
                        </li>
                        <li>
                            <a href="/respuesta">
                                <i class="material-icons">content_paste</i>
                                <p>Creación de Respuestas</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/modulo') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/usuario">
                                <i class="material-icons">person</i>
                                <p>Crear Usuario</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/modulo">
                                <i class="material-icons">create</i>
                                <p>Creación de Módulos</p>
                            </a>
                        </li>
                        <li>
                            <a href="/pregunta">
                                <i class="material-icons">question_answer</i>
                                <p>Creación de Preguntas</p>
                            </a>
                        </li>
                        <li>
                            <a href="/respuesta">
                                <i class="material-icons">content_paste</i>
                                <p>Creación de Respuestas</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/pregunta') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/usuario">
                                <i class="material-icons">person</i>
                                <p>Crear Usuario</p>
                            </a>
                        </li>
                        <li>
                            <a href="/modulo">
                                <i class="material-icons">create</i>
                                <p>Creación de Módulos</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/pregunta">
                                <i class="material-icons">question_answer</i>
                                <p>Creación de Preguntas</p>
                            </a>
                        </li>
                        <li>
                            <a href="/respuesta">
                                <i class="material-icons">question_answer</i>
                                <p>Creación de Respuestas</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/respuesta') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/usuario">
                                <i class="material-icons">person</i>
                                <p>Crear Usuario</p>
                            </a>
                        </li>
                        <li>
                            <a href="/modulo">
                                <i class="material-icons">create</i>
                                <p>Creación de Módulos</p>
                            </a>
                        </li>
                        <li>
                            <a href="/pregunta">
                                <i class="material-icons">question_answer</i>
                                <p>Creación de Preguntas</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/respuesta">
                                <i class="material-icons">content_paste</i>
                                <p>Creación de Respuestas</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        @endif
        @if (Auth::user()->rol == "invitado")
            <div class="sidebar" data-color="green" data-image="/img/sidebar-1.jpg">
                <!--
                    Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

                    Tip 2: you can also add an image using data-image tag
                -->

                <div class="logo">
                    <a href="http://www.creative-tim.com" class="simple-text">
                        Globallynx
                    </a>
                </div>

                <div class="sidebar-wrapper">
                    <ul class="nav">
                        @if(($_SERVER["REQUEST_URI"]) == '/home') 
                        <li class="active">
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/1">
                                <i class="material-icons">dashboard</i>
                                <p>Importancia de la Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/2">
                                <i class="material-icons">dashboard</i>
                                <p>Bases de Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/3">
                                <i class="material-icons">dashboard</i>
                                <p>Principales Amenazas y Riesgos.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/4">
                                <i class="material-icons">dashboard</i>
                                <p>Medidas preventivas de seguridad de información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/5">
                                <i class="material-icons">dashboard</i>
                                <p>Escenarios de riesgo de seguridad de información.</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/questionario/1') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/questionario/1">
                                <i class="material-icons">dashboard</i>
                                <p>Importancia de la Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/2">
                                <i class="material-icons">dashboard</i>
                                <p>Bases de Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/3">
                                <i class="material-icons">dashboard</i>
                                <p>Principales Amenazas y Riesgos.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/4">
                                <i class="material-icons">dashboard</i>
                                <p>Medidas preventivas de seguridad de información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/5">
                                <i class="material-icons">dashboard</i>
                                <p>Escenarios de riesgo de seguridad de información.</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/questionario/2') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/1">
                                <i class="material-icons">dashboard</i>
                                <p>Importancia de la Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/questionario/2">
                                <i class="material-icons">dashboard</i>
                                <p>Bases de Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/3">
                                <i class="material-icons">dashboard</i>
                                <p>Principales Amenazas y Riesgos.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/4">
                                <i class="material-icons">dashboard</i>
                                <p>Medidas preventivas de seguridad de información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/5">
                                <i class="material-icons">dashboard</i>
                                <p>Escenarios de riesgo de seguridad de información.</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/questionario/3') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/1">
                                <i class="material-icons">dashboard</i>
                                <p>Importancia de la Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/2">
                                <i class="material-icons">dashboard</i>
                                <p>Bases de Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/questionario/3">
                                <i class="material-icons">dashboard</i>
                                <p>Principales Amenazas y Riesgos.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/4">
                                <i class="material-icons">dashboard</i>
                                <p>Medidas preventivas de seguridad de información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/5">
                                <i class="material-icons">dashboard</i>
                                <p>Escenarios de riesgo de seguridad de información.</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/questionario/4') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/1">
                                <i class="material-icons">dashboard</i>
                                <p>Importancia de la Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/2">
                                <i class="material-icons">dashboard</i>
                                <p>Bases de Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/3">
                                <i class="material-icons">dashboard</i>
                                <p>Principales Amenazas y Riesgos.</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/questionario/4">
                                <i class="material-icons">dashboard</i>
                                <p>Medidas preventivas de seguridad de información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/5">
                                <i class="material-icons">dashboard</i>
                                <p>Escenarios de riesgo de seguridad de información.</p>
                            </a>
                        </li>
                        @endif
                        @if(($_SERVER["REQUEST_URI"]) == '/questionario/5') 
                        <li>
                            <a href="/home">
                                <i class="material-icons">dashboard</i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/1">
                                <i class="material-icons">dashboard</i>
                                <p>Importancia de la Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/2">
                                <i class="material-icons">dashboard</i>
                                <p>Bases de Seguridad de la Información.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/3">
                                <i class="material-icons">dashboard</i>
                                <p>Principales Amenazas y Riesgos.</p>
                            </a>
                        </li>
                        <li>
                            <a href="/questionario/4">
                                <i class="material-icons">dashboard</i>
                                <p>Medidas preventivas de seguridad de información.</p>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/questionario/5">
                                <i class="material-icons">dashboard</i>
                                <p>Escenarios de riesgo de seguridad de información.</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        @endif 
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'Segob') }}
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                                <li class="dropdown">
                                    <li><a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('login') }}">Login</a></li>
                                    
                                </li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            @yield('content')
        </div>
    </div>
    <!--   Core JS Files   -->

    <script src="/js/jquery-3.1.0.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.2.0/ekko-lightbox.js"></script>
    
    <script src="/js/material.min.js" type="text/javascript"></script>

    <!--  Charts Plugin -->
    <script type="text/javascript" src="/js/Chart.js"></script>

    <!--  Notifications Plugin    -->
    <script type="text/javascript" src="/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Material Dashboard javascript methods -->
    <script type="text/javascript" src="/js/material-dashboard.js"></script>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">  
@stack('scripts')
</body>
</html>
