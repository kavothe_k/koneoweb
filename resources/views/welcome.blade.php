<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Koneo') }}</title>

    <!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="/inicio/css/base.css">
   <link rel="stylesheet" href="/inicio/css/vendor.css">  
   <link rel="stylesheet" href="/inicio/css/main.css">  

   <!-- script
   ================================================== -->
    <script src="/inicio/js/modernizr.js"></script>
    <script src="/inicio/js/pace.min.js"></script>

   <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top">

    <!-- header 
   ================================================== -->
    <header>
        <div class="header-logo">
            <a href="">koneo</a>
        </div>
    </header> <!-- end header -->


   <!-- home
   ================================================== -->
   <section id="home">

    <div class="overlay"></div>

    <div class="home-content-table">    
           <div class="home-content-tablecell">
            <div class="row">
                <div class="col-twelve">                    
                    
                            <h3 class="animate-intro">Ya nos conoces.</h3>
                            <h1 class="animate-intro">
                            Prueba nuestras  <br>
                            Soluciones.
                            </h1>   

                            <div class="more animate-intro">
                                <a class="smoothscroll button stroke" href="#about">
                                    Leer más
                                </a>
                            </div>                          

                    </div> <!-- end col-twelve --> 
            </div> <!-- end row --> 
           </div> <!-- end home-content-tablecell -->          
        </div> <!-- end home-content-table -->

        <ul class="home-social-list">
          <li class="animate-intro">
                <a href="#"><i class="fa fa-facebook-square"></i></a>
          </li>
          <li class="animate-intro">
                <a href="#"><i class="fa fa-twitter"></i></a>
          </li>
          <li class="animate-intro">
                <a href="#"><i class="fa fa-instagram"></i></a>
          </li>
         <li class="animate-intro">
            <a href="#"><i class="fa fa-behance"></i></a>
         </li>
          <li class="animate-intro">
                <a href="#"><i class="fa fa-dribbble"></i></a>
          </li>       
       </ul> <!-- end home-social-list -->  

        <div class="scrolldown">
            <a href="#about" class="scroll-icon smoothscroll">      
            Scroll Down         
            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </a>
        </div>          
   
   </section> <!-- end home -->


   <!-- about
   ================================================== -->
   <section id="about">

    <div class="row about-wrap">
        <div class="col-full">

            <div class="about-profile-bg"></div>

                <div class="intro">
                    <h3 class="animate-this">Lorem ipsum</h3>
                <p class="lead animate-this"><span>Lorem ipsum</span> dolor sit amet, consectetur adipiscing elit. Sed sed felis aliquet, pulvinar lacus in, lacinia libero. Morbi sed eleifend nulla. Ut euismod tellus nec magna lobortis, at ultrices nisi congue.</p> 
                </div>   

        </div> <!-- end col-full  -->
    </div> <!-- end about-wrap  -->

   </section> <!-- end about -->


   <!-- about
   ================================================== -->
   <section id="services">

    <div class="overlay"></div>
    <div class="gradient-overlay"></div>
    
    <div class="row narrow section-intro with-bottom-sep animate-this">
        <div class="col-full">
            
                <h3>Servicios</h3>
               <h1>¿Qué Ofrecemos?.</h1>
            
               <p class="lead">Lorem ipsum Elit ut consequat veniam eu nulla nulla reprehenderit reprehenderit sit velit in cupidatat ex aliquip ut cupidatat Excepteur tempor id irure sed dolore sint sunt voluptate ullamco nulla qui Duis qui culpa voluptate enim ea aute qui veniam in irure et nisi nostrud deserunt est officia minim.</p>
            
       </div> <!-- end col-full -->
    </div> <!-- end row -->

    <div class="row services-content">

        <div class="services-list block-1-2 block-tab-full group">

            <div class="bgrid service-item animate-this">   

                <span class="icon"><i class="icon-paint-brush"></i></span>            

                <div class="service-content">
                    <h3 class="h05">Branding</h3>

                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                    </p>                    
                </div>               

                </div> <!-- end bgrid -->

                <div class="bgrid service-item animate-this">   

                    <span class="icon"><i class="icon-earth"></i></span>                          

                <div class="service-content">   
                    <h3 class="h05">Diseño Web</h3>

                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                    </p>                    
                </div>                            

               </div> <!-- end bgrid -->

               <div class="bgrid service-item animate-this">

                <span class="icon"><i class="icon-lego-block"></i></span>                   

                <div class="service-content">
                    <h3 class="h05">Desarrollo Web</h3>

                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                        </p>
                </div>                                 

               </div> <!-- end bgrid -->

                <div class="bgrid service-item animate-this">

                    <span class="icon"><i class="icon-megaphone"></i></span>                  

                <div class="service-content">
                    <h3 class="h05">Marketing</h3>

                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                    </p>                    
                </div>                

                </div> <!-- end bgrid -->              

          </div> <!-- end services-list -->
        
    </div> <!-- end services-content -->            

   </section> <!-- end services -->

   <!-- Testimonials Section
   ================================================== -->
   <section id="testimonials">

    <div class="row">
        <div class="col-twelve">
            <h2 class="animate-this">Lo que dicen sobre nosotros.</h2>
        </div>          
    </div>      

      <div class="row flex-container">
    
         <div id="testimonial-slider" class="flex-slider animate-this">

            <ul class="slides">

               <li>
                  <p>
                  Contar con especialistas en la materia otorga tranquilidad. El diseño de procesos y su guía para implementarlos fue fundamental en nuestro proyecto de implementación de mejores prácticas.
                  </p> 

                  <div class="testimonial-author">
                        <img src="/inicio/images/avatars/user-16.jpg" alt="Author image">
                        <div class="author-info">
                           Rodrigo Zarate,
                            <span class="position">Genexus.</span>
                        </div>
                  </div>                 
                </li> <!-- end slide -->

               <li>
                  <p>
                  Su experiencia en la gestión de servicios y herramientas para la gestión, permitió que un proyecto complejo se volviera más fácil de lograr con resultados como la optimización de la operación de la Mesa de Servicios, que proyectó un ahorro anual de 30 millones de pesos.
                  </p>

                <div class="testimonial-author">
                        <img src="/inicio/images/avatars/user-13.jpg" alt="Author image">
                        <div class="author-info">
                            Jazmin,
                            <span>IBM.</span>
                        </div>
                  </div>                                         
               </li> <!-- end slide -->

            </ul> <!-- end slides -->

         </div> <!-- end testimonial-slider -->         
        
      </div> <!-- end flex-container -->

   </section> <!-- end testimonials -->


    <!-- stats
   ================================================== -->
   <section id="clients">

        <div class="row animate-this">
            <div class="col-twelve">
                <div class="row">
                    <div class="col-twelve">
                        <h2 class="animate-this">Tegnología Utilizada.</h2>
                    </div>          
                </div> 
                <div class="client-lists owl-carousel">
                    <div><img src="/inicio/images/clients/mozilla.png" alt=""></div>
                    <div><img src="/inicio/images/clients/bower.png" alt=""></div>
                    <div><img src="/inicio/images/clients/codepen.png" alt=""></div>
                    <div><img src="/inicio/images/clients/envato.png" alt=""></div>
                    <div><img src="/inicio/images/clients/firefox.png" alt=""></div>
                    <div><img src="/inicio/images/clients/grunt.png" alt=""></div>
                    <div><img src="/inicio/images/clients/evernote.png" alt=""></div>
                    <div><img src="/inicio/images/clients/github.png" alt=""></div>
                    <div><img src="/inicio/images/clients/joomla.png" alt=""></div>
                    <div><img src="/inicio/images/clients/jQuery.png" alt=""></div>
                    <div><img src="/inicio/images/clients/wordpress.png" alt=""></div>
                </div>
                
            </div> <!-- end col-twelve -->
        </div> <!-- end row -->

   </section> <!-- end clients -->


    <!-- contact
   ================================================== -->
   <section id="contact">

      <div class="overlay"></div>

        <div class="row narrow section-intro with-bottom-sep animate-this">
        <div class="col-twelve">
            <h3>Contacto</h3>
            <h1>Estar en contacto.</h1>

            <p class="lead">Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
        </div> 
    </div> <!-- end section-intro -->

    <div class="row contact-content">

        <div class="col-seven tab-full animate-this">

            <h5>Envianos un mensaje</h5>

            <!-- form -->
            <form name="contactForm" id="contactForm" method="post">                

               <div class="form-field">
                       <input name="contactName" type="text" id="contactName" placeholder="Nombre" value="" minlength="2" required="">
               </div>

               <div class="row">
                    <div class="col-six tab-full">
                        <div class="form-field">
                            <input name="contactEmail" type="email" id="contactEmail" placeholder="Email" value="" required="">
                        </div>                         
                    </div>
                    <div class="col-six tab-full">              
                        <div class="form-field">
                            <input name="contactSubject" type="text" id="contactSubject" placeholder="Asunto" value="">
                      </div>                               
                    </div>
               </div>
                                         
               <div class="form-field">
                    <textarea name="contactMessage" id="contactMessage" placeholder="Mensaje" rows="10" cols="50" required=""></textarea>
                </div> 

               <div class="form-field">
                  <button class="submitform">Enviar</button>

                  <div id="submit-loader">
                     <div class="text-loader">Sending...</div>                             
                      <div class="s-loader">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
               </div>

            </form> <!-- end form -->

            <!-- contact-warning -->
            <div id="message-warning"></div> 

            <!-- contact-success -->
            <div id="message-success">
               <i class="fa fa-check"></i>Tu mensaje fue enviado, ¡Gracias!<br>
            </div>

         </div> <!-- end col-seven --> 

         <div class="col-four tab-full contact-info end animate-this">

            <h5>Información de contacto</h5>

            <div class="cinfo">
                <h6>Nos encuentras en</h6>
                <p>
                    Calzada de La Viga #550BIS<br>
                    Col. Santa Anita, Del. Iztacalco<br>
                    C.P.08300, Ciudad de México
                </p>
            </div> <!-- end cinfo -->

            <div class="cinfo">
                <h6>Envianos correo a</h6>
                <p>
                    contacto@koneo.mx<br>
                </p>
            </div> <!-- end cinfo -->

            <div class="cinfo">
                <h6>Llamanos a</h6>
                <p>
                    Phone: (+52) 1 55 2710 9778<br>
                    Mobile: (+52) 1 55 2710 9778<br>
                    Fax: (+52) 1 55 2710 9778
                   </p>
            </div>

         </div> <!-- end cinfo --> 

    </div> <!-- end row contact-content -->
        
    </section> <!-- end contact -->

      <div id="go-top">
           <a class="smoothscroll" title="Back to Top" href="#top">
            <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
           </a>
        </div>      
   </footer>

   <div id="preloader"> 
        <div id="loader"></div>
   </div> 

   <!-- Java Script
   ================================================== --> 
   <script src="/inicio/js/jquery-2.1.3.min.js"></script>
   <script src="/inicio/js/plugins.js"></script>
   <script src="/inicio/js/main.js"></script>

</body>

</html>